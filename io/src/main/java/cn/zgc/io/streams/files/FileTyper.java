package cn.zgc.io.streams.files;

import cn.zgc.io.streams.StreamCopier;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileTyper {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: java FileTyper filename");
            return;
        }
        typeFile(args[0]);
    }

    public static void typeFile(String fileName) throws IOException {
        System.out.println("开始文件拷贝...fileName="+fileName);
        try(InputStream is = new FileInputStream(fileName)){
            StreamCopier streamCopier = new StreamCopier();
            //FileOutputStream fos = new FileOutputStream("D:\\a.txt");
            streamCopier.copy(is,System.out);
        }
    }
}
