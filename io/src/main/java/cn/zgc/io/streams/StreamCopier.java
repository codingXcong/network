package cn.zgc.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamCopier {

    private byte[] buffer = new byte[1024];

    public static void main(String[] args) {
        byte a = (byte)234;
        int i = a;
        System.out.println(Integer.toBinaryString(i));
        int j = a & 0xff;
        System.out.println(Integer.toBinaryString(j));
    }

    public void copy(InputStream inputStream , OutputStream outputStream) throws IOException {
        for(;;){
            int readLength = inputStream.read(buffer);
            if(readLength == -1) {
                break;
            }
            outputStream.write(buffer,0,readLength);
        }
    }

    public StreamCopier setBufferSize(int bufferSize){
        buffer = new byte[bufferSize];
        return this;
    }
}
