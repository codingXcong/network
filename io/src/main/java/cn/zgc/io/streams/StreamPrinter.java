package cn.zgc.io.streams;

import java.io.IOException;

public class StreamPrinter {
    public static void main(String[] args) {
        try {
            for(;;) {
                int read = System.in.read();
                if(read == -1){
                    break;
                }
                System.out.println(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
