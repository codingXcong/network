package cn.zgc.io.streams.files;

import cn.zgc.io.streams.StreamCopier;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopier {
    public static void main(String[] args) {

        if (args.length != 2) {
            System.err.println("Usage: java FileCopier infile outfile");
        }
        try {
            copy(args[0], args[1]);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public static void copy(String inFile, String outFile)
            throws IOException {

        FileInputStream fin = null;
        FileOutputStream fout = null;

        try {
            fin  = new FileInputStream(inFile);
            fout = new FileOutputStream(outFile);
            new StreamCopier().copy(fin, fout);
        }
        finally {
            try {
                if (fin != null) fin.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                if (fout != null) fout.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
