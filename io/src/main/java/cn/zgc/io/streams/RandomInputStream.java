package cn.zgc.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class RandomInputStream extends InputStream{

    private Random random = new Random();
    private boolean closed = false;

    @Override
    public int read() throws IOException {
        checkOpen();
        int result = random.nextInt() % 256;
        return Math.abs(result);
    }

    @Override
    public int read(byte[] b) throws IOException {
        checkOpen();
        random.nextBytes(b);
        return b.length;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        checkOpen();
        byte[] tmp = new byte[len];
        random.nextBytes(tmp);
        System.arraycopy(tmp,0,b,off,len);
        return len;
    }

    @Override
    public long skip(long n) throws IOException {
        checkOpen();
        return n;
    }

    @Override
    public void close() throws IOException {
        this.closed = true ;
    }

    private void checkOpen() throws IOException {
        if(closed){
            throw new IOException("RandomInputStream closed");
        }
    }
}
