package cn.zgc.netty.demo.discard;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * The most simplistic protocol in the world is not 'Hello, World!' but DISCARD.
 * It's a protocol which discards any received data without any response.
 */
public class DiscardServerHandler extends ChannelInboundHandlerAdapter {
    /**
     * Please keep in mind that it is the handler's responsibility to release any reference-counted object passed to the handler.
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ((ByteBuf)msg).release();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
